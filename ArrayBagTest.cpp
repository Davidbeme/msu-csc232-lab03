/**
 * @file   ArrayBagTest.cpp
 * @author Jim Daehn
 * @brief  TODO: Give brief description of this class.
 */

#include "ArrayBagTest.h"
#include "ArrayBag.h"

CPPUNIT_TEST_SUITE_REGISTRATION(ArrayBagTest);

ArrayBagTest::ArrayBagTest() {
}

ArrayBagTest::~ArrayBagTest() {
}

void ArrayBagTest::setUp() {
	bag.add("a");
	bag.add("b");
	bag.add("c");

	anotherBag.add("b");
	anotherBag.add("b");
	anotherBag.add("d");
	anotherBag.add("e");
}

void ArrayBagTest::tearDown() {
}

void ArrayBagTest::testUnionHasExpectedFrequencyOfA() {
	expectedBag.add("a");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("c");
	expectedBag.add("d");
	expectedBag.add("e");

	actualBag = bag.getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("a"), actualBag.getFrequencyOf("a"));
}

void ArrayBagTest::testUnionHasExpectedFrequencyOfB() {
	expectedBag.add("a");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("c");
	expectedBag.add("d");
	expectedBag.add("e");

	actualBag = bag.getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("b"), actualBag.getFrequencyOf("b"));
}

void ArrayBagTest::testUnionHasExpectedFrequencyOfC() {
	expectedBag.add("a");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("c");
	expectedBag.add("d");
	expectedBag.add("e");

	actualBag = bag.getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("c"), actualBag.getFrequencyOf("c"));
}

void ArrayBagTest::testUnionHasExpectedFrequencyOfD() {
	expectedBag.add("a");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("c");
	expectedBag.add("d");
	expectedBag.add("e");

	actualBag = bag.getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("d"), actualBag.getFrequencyOf("d"));
}

void ArrayBagTest::testUnionHasExpectedFrequencyOfE() {
	expectedBag.add("a");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("b");
	expectedBag.add("c");
	expectedBag.add("d");
	expectedBag.add("e");

	actualBag = bag.getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("e"), actualBag.getFrequencyOf("e"));
}

void ArrayBagTest::testIntersectionHasExpectedFrequencyOfA() {
	expectedBag.add("b");

	actualBag = bag.getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("a"), actualBag.getFrequencyOf("a"));
}

void ArrayBagTest::testIntersectionHasExpectedFrequencyOfB() {
	expectedBag.add("b");

	actualBag = bag.getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("b"), actualBag.getFrequencyOf("b"));
}

void ArrayBagTest::testIntersectionHasExpectedFrequencyOfC() {
	expectedBag.add("b");

	actualBag = bag.getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("c"), actualBag.getFrequencyOf("c"));
}

void ArrayBagTest::testIntersectionHasExpectedFrequencyOfD() {
	expectedBag.add("b");

	actualBag = bag.getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("d"), actualBag.getFrequencyOf("d"));
}

void ArrayBagTest::testIntersectionHasExpectedFrequencyOfE() {
	expectedBag.add("b");

	actualBag = bag.getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("e"), actualBag.getFrequencyOf("e"));
}

void ArrayBagTest::testDifferenceHasExpectedFrequencyOfA() {
	expectedBag.add("a");
	expectedBag.add("c");

	actualBag = bag.getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("a"), actualBag.getFrequencyOf("a"));
}

void ArrayBagTest::testDifferenceHasExpectedFrequencyOfB() {
	expectedBag.add("a");
	expectedBag.add("c");

	actualBag = bag.getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("b"), actualBag.getFrequencyOf("b"));
}

void ArrayBagTest::testDifferenceHasExpectedFrequencyOfC() {
	expectedBag.add("a");
	expectedBag.add("c");

	actualBag = bag.getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("c"), actualBag.getFrequencyOf("c"));
}

void ArrayBagTest::testDifferenceHasExpectedFrequencyOfD() {
	expectedBag.add("a");
	expectedBag.add("c");

	actualBag = bag.getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("d"), actualBag.getFrequencyOf("d"));
}

void ArrayBagTest::testDifferenceHasExpectedFrequencyOfE() {
	expectedBag.add("a");
	expectedBag.add("c");

	actualBag = bag.getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag.getFrequencyOf("e"), actualBag.getFrequencyOf("e"));
}
