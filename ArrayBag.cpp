/**
 * @file   ArrayBag.cpp
 * @author Sam Young, David Williams
 * @brief  Implementation of ArrayBag.
 */

#include "ArrayBag.h"

template<typename ItemType>
ArrayBag<ItemType>::ArrayBag() :
		itemCount(0), maxItems(DEFAULT_CAPACITY) {
	// Intentionally empty body
}

template<typename ItemType>
bool ArrayBag<ItemType>::add(const ItemType& newEntry) {
	bool hasRoomToAdd = (itemCount < maxItems);
	if (hasRoomToAdd) {
		items[itemCount] = newEntry;
		++itemCount;
	}

	return hasRoomToAdd;
}

template<class ItemType>
std::vector<ItemType> ArrayBag<ItemType>::toVector() const {
	std::vector<ItemType> bagContents;
	for (int i = 0; i < itemCount; ++i) {
		bagContents.push_back(items[i]);
	}
	return bagContents;
}

template<class ItemType>
int ArrayBag<ItemType>::getCurrentSize() const {
	return itemCount;
}

template<class ItemType>
bool ArrayBag<ItemType>::isEmpty() const {
	return itemCount == 0;
}

template<class ItemType>
bool ArrayBag<ItemType>::remove(const ItemType& anEntry) {
	int locatedIndex{getIndexOf(anEntry, 0)};
	bool canRemoveItem{!isEmpty() && (locatedIndex > -1)};
	if (canRemoveItem) {
		--itemCount;
		items[locatedIndex] = items[itemCount];
	}
	return canRemoveItem;
}

template<class ItemType>
void ArrayBag<ItemType>::clear() {
	itemCount = 0;
}

template<class ItemType>
bool ArrayBag<ItemType>::contains(const ItemType& anEntry) const {
	bool found{false};
	int curIndex{0};
	while (!found && (curIndex < itemCount)) {
		found = (anEntry == items[curIndex]);
		if (!found) {
			++curIndex;
		}
	}
	return found;
}

template<class ItemType>
int ArrayBag<ItemType>::getFrequencyOf(const ItemType& anEntry) const {
	int frequency{0};
	int curIndex{0};

	while (curIndex < itemCount) {
		if (items[curIndex] == anEntry) {
			++frequency;
		}
		++curIndex;
	}
	return frequency;
}

template<class ItemType>
int ArrayBag<ItemType>::getIndexOf(const ItemType& target, int searchIndex) const {
	int result{INDEX_NOT_FOUND};
	if (searchIndex < itemCount) {
		if (items[searchIndex] == target) {
			result = searchIndex;
		} else {
			result = getIndexOf(target, searchIndex + 1);
		}
	}
	return result;
}

template<class ItemType>
int ArrayBag<ItemType>::countFrequency(const ItemType& target, int searchIndex) const {
	int frequency{0};
	if (searchIndex < itemCount) {
		if (items[searchIndex] == target) {
			frequency = 1 + countFrequency(target, searchIndex + 1);
		} else {
			frequency = countFrequency(target, searchIndex + 1);
		}
	}
	return frequency;
}

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::getUnionWithBag(ArrayBag<ItemType> aBag) {
	ArrayBag<ItemType> bag;
	
	std::vector<ItemType> bag1Vector = this->toVector();
	std::vector<ItemType> bag2Vector = aBag.toVector();
	
	for (ItemType bagItem:bag1Vector) {
		bag.add(bagItem);
	}
	for (ItemType bagItem:bag2Vector) {
		bag.add(bagItem);
	}

	return bag;
}

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::getIntersectionWithBag(ArrayBag<ItemType> aBag) {
	ArrayBag<ItemType> bag;

	std::vector<ItemType> bag1Vector = this->toVector();
	
	for (ItemType bagItem:bag1Vector) {
		if (aBag.contains(bagItem)) {
			bag.add(bagItem);
		}
	}

	return bag;
}

template<class ItemType>
ArrayBag<ItemType> ArrayBag<ItemType>::getDifferenceWithBag(ArrayBag<ItemType> aBag) {
	ArrayBag<ItemType> bag;

	std::vector<ItemType> bag1Vector = this->toVector();
	
	for (ItemType bagItem:bag1Vector) {
		if (!aBag.contains(bagItem)) {
			bag.add(bagItem);
		}
	}

	return bag;
}
